exports.isObject = object => object !== null && object.constructor.name === 'Object';

// eslint-disable-next-line unicorn/explicit-length-check
exports.isObjectEmpty = object => !Object.keys(object).length;

exports.findDefault = object => Object.keys(object).find(i => i.split(':').length > 1);
